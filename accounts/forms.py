from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from accounts.models import UserProfile


class EditProfileForm(UserChangeForm):
    template_name='edit_profile'

    class Meta:
        model = User
        fields = (
            'email',
            'password'
)